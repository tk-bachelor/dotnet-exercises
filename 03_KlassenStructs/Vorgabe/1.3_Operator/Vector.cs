﻿using System;

namespace _1._3_Operator
{
    class Vector
    {
        public int X { get;  set; }
        public int Y { get; set; }

        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }

        // TODO: Operator "+" implementieren
        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X+b.X, a.Y+b.Y);
        }

        // TODO: Operator "-" implementieren
        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        // TODO: Operator "==" implementieren
        public static bool operator ==(Vector a, Vector b)
        {
            return (a.X == b.X && a.Y == b.Y);
        }



        // TODO: Operator "!=" implementieren
        public static bool operator !=(Vector a, Vector b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return $"X: {X} / Y: {Y}";
        }
    }
}
