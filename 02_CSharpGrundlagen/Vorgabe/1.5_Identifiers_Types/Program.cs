﻿using System;

namespace _1._5_Identifiers_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            TestSymbols();
            TestPrimitiveTypes();

            Console.ReadKey();
        }

        private static void TestSymbols()
        {
            // TODO: Erstellen Sie eine int-Variable mit dem Namen "null"
            int @null = 0;

            // TODO: Weisen Sie der Variable den Wert 1 zu
            @null = 1;

            // TODO: Geben Sie den Wert auf die Konsole aus
            Console.WriteLine($"Null value is {@null}");
        }

        private static void TestPrimitiveTypes()
        {
            char c = 'a';
            int i = 255;

            // TODO: Weisen Sie "c" einer neuen decimal-Variable "d" zu
            decimal d = c;

            // TODO: Weisen Sie "i" einer neuen sbyte-Variable "s" zu
            sbyte s = (sbyte)i;

            // TODO: Geben Sie den Wert auf die Konsole aus
            Console.WriteLine(s);

            // TODO: Optional / Überlegen Sie sich, wieso das gegebene Resultat entsteht (Hinweis: Bitlogik!)
            // Lösung:
            // i    = 32bit    = 255      = 0000 0000 0000 0000 0000 0000 1111 1111
            // s    = 8bit     = -1       =                               1111 1111
            //
            // 1. Bit ist das Vorzeichen 
            //          Hier: Vorzeichen = 1 (MINUS)
            // 2. - 8. Bit werden vom tiefsten Negativwert (128) abgezogen
            //          Hier: 128 - 127 (0111 1111) = 1
        }
    }
}
