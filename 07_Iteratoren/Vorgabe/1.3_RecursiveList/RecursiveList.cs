﻿using System;
using System.Collections.Generic;

namespace _1._3_RecursiveList
{
    public class RecursiveList<T>
    {
        public class Node
        {
            public Node Next { get; set; }
            public T Value { get; private set; }
            public Node(T val) { Value = val; }
        }

        private Node root;
        private Node tail;

        public RecursiveList()
        {
            root = new Node(default(T));
            tail = root;
        }
        public void Append(T val)
        {
            tail.Next = new Node(val);
            tail = tail.Next;
        }

        public IEnumerable<T> TraverseIterative
        {
            get
            {
                Node current = root;
                while (current.Next != null)
                {
                    current = current.Next;
                    yield return current.Value;
                }
            }
        }

        public IEnumerable<T> Traverse
        {
            get
            {
                return PreorderTraverse(root.Next);
            }
        }

        private IEnumerable<T> PreorderTraverse(Node curr)
        {
            if(curr != null)
            {
                yield return curr.Value;
                foreach(T t in PreorderTraverse(curr.Next))
                {
                    yield return t;
                }
            }
        }

        public IEnumerable<T> Inverse
        {
            get
            {
                return PostorderTraverse(root.Next);
            }
        }

        private IEnumerable<T> PostorderTraverse(Node curr)
        {
            if(curr != null)
            {
                foreach(T t in PostorderTraverse(curr.Next))
                {
                    yield return t;
                }
                yield return curr.Value;
            }
        }

        public void ForEach(Action<T> action)
        {
            if (action == null) return;

            foreach(T elem in Traverse)
            {
                action(elem);
            }
        }
    }
}
