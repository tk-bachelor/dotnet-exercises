﻿using System;
using System.Collections.Generic;

namespace _2._1_ExtensionMethodsSimple
{
    public static class Extensions
    {
        /****** HsrMultipleOf ******/

        // TODO: Operator 'HsrMultipleOf' implementieren / Gibt alle Werte zurück, bei denen "x % factor" == 0 ist
        public static IEnumerable<int> HsrMultipleOf(this IEnumerable<int> source, int factor)
        {
            foreach(int x in source)
            {
                if (x % factor == 0)
                {
                    yield return x;
                }
            }
        }

        /****** HsrWhere ******/

        // TODO: Operator 'HsrWhere' implementieren / Gibt alle Werte zurück, bei denen ein "Predicate<T>" true liefert
        public static IEnumerable<T> HsrWhere<T>(this IEnumerable<T> source, Predicate<T> predicate)
        {
            foreach(T t in source)
            {
                if (predicate(t))
                {
                    yield return t;
                }
            }
        }

    }
}
