﻿using System;
using System.Collections.Generic;

namespace _1._1_Fibonacci
{
    public class Math
    {
        // TODO: Implementiere Methode Fibonacci
        public static IEnumerable<int> Fibonacci(int n)
        {
            int prev = 0;
            yield return prev;

            int next = 1;
            yield return next;

            int sum;
            for(int i=2; i<8; i++)
            {
                sum = prev + next;
                prev = next;
                next = sum;

                yield return sum;
            }
        }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            foreach (int i in Math.Fibonacci(8))
            {
                Console.WriteLine("{0} ", i);
            }

            Console.ReadKey();
        }
    }
}
