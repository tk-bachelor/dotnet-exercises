using System;

namespace MultipleAssemblies
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Hello from Console / Program.Main()");
            HelloConsole.Speak();
        }
    }
}
