﻿using System;              // For Console, Int32, ArgumentException, Environment
using System.Net;          // For IPAddress
using System.Net.Sockets;  // For TcpListener, TcpClient
using System.Text;

namespace SocketServer
{
    class Program
    {
        private const int BufferSize = 200; // Size of receive buffer
        private const int Backlog = 5;  // Outstanding connection queue max size

        static void Main(string[] args)
        {
            // Test for correct # of args
            if (args.Length > 1)
            {
                throw new ArgumentException("Parameters: [<Port>]", nameof(args));
            }

            int servPort = args.Length == 1
                ? int.Parse(args[0]) 
                : 1000;

            Socket server = null;
            
            try
            {
                // Create a socket to accept client connections
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Bind(new IPEndPoint(IPAddress.Any, servPort));
                server.Listen(Backlog);
            }
            catch (SocketException se)
            {
                Console.WriteLine(se.ErrorCode + ": " + se.Message);
                Environment.Exit(se.ErrorCode);
            }

            byte[] receiveBuffer = new byte[BufferSize];

            // Run forever, accepting and servicing connections
            for (;;)
            {
                Socket client = null;

                try
                {
                    // Get client connection
                    client = server.Accept(); 

                    Console.Write("Handling client at " + client.RemoteEndPoint + " - ");

                    // Receive until client closes connection, indicated by 0 return value
                    int totalBytesEchoed = 0;

                    int bytesReceived;
                    while ((bytesReceived = client.Receive(receiveBuffer, 0, receiveBuffer.Length, SocketFlags.None)) > 0)
                    {
                        totalBytesEchoed += bytesReceived;
                        Console.Write(Encoding.ASCII.GetString(receiveBuffer, 0, bytesReceived));
                    }
                    Console.WriteLine("Received {0} bytes.", totalBytesEchoed);

                    // Close the socket
                    client.Close();   
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    client.Close();
                }
            }
        }
    }
}
