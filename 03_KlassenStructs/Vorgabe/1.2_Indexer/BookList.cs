﻿using System;

namespace _1._2_Indexer
{
    class BookList
    {
        private string[,] books =
        {
            {"The Green Mile", "Stephen King"},
            {"It", "Stephen King"},
            {"Misery", "Stephen King"},
            {"The Raven", "Edgar Allan Poe"}
        };

        // TODO: Indexer
        public string this[int index]
        {
            get
            {
                return string.Format("title:{0} / author: {1}", books[index,0], books[index,1]);
            }
        }

        public string this[string index]
        {
            get
            {
                for(int i=0; i<books.Length; i++)
                {
                    // first column is the book name
                    if (books[i, 0].Equals(index))
                    {
                        // second column is the author
                        return books[i, 1];
                    }
                }
                return null;
            }
        }
    }
}
