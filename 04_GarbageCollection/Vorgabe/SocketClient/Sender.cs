﻿using System;              // For String, Int32, Console, ArgumentException
using System.IO;           // For IOException
using System.Net;          // For IPAddress, IPEndPoint
using System.Net.Sockets;  // For Socket, SocketException

namespace SocketClient
{
    public class Sender : IDisposable
    {
        private Socket socket;
        private bool isOpen;

        public void Connect(string server = "localhost", int serverPort = 1000)
        {
            // Create a TCP socket instance 
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var ipAddr = Dns.GetHostEntry(server).AddressList[1];

            // Creates server IPEndPoint instance. We assume Resolve returns at least one address
            IPEndPoint serverEndPoint = new IPEndPoint(ipAddr, serverPort);

            // Connect the socket to server on specified port
            socket.Connect(serverEndPoint);
            isOpen = true;
            Console.WriteLine("Connected to server");
        }

        public void Close()
        {
            if (isOpen)
            {
                socket.Close();
            }
        }

        public void Send(string fileName)
        {
            if (!isOpen)
            {
                throw new ApplicationException("Sending file not possible, call Open first");
            }

            try
            {
                using (FileStream fsSource = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    // Read the source file into a byte array.
                    byte[] bytes = new byte[fsSource.Length];
                    int numBytesToRead = (int)fsSource.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        // Read may return anything from 0 to numBytesToRead.
                        int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);

                        // Break when the end of the file is reached.
                        if (n == 0)
                            break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }

                    // Send the  string to the server
                    socket.Send(bytes, 0, numBytesRead, SocketFlags.None);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    socket.Close();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Sender() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
