﻿
using System;              // For String, Int32, Console, ArgumentException
using System.IO;           // For IOException
using System.Net;          // For IPAddress, IPEndPoint
using System.Net.Sockets;  // For Socket, SocketException
using System.Text;

namespace SocketClient
{
    public class Sender : IDisposable
    {
        private Socket socket;
        private StreamWriter fsLog;
        private bool isOpen;
        private const string logFileName= @"..\..\log.txt";

        public Sender() {
            fsLog = new StreamWriter(logFileName);
        }
        public void Connect(string server = "localhost", int serverPort = 1000)
        {
            // Create a TCP socket instance 
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var ipAddr = Dns.GetHostEntry(server).AddressList[1];

            // Creates server IPEndPoint instance. We assume Resolve returns at least one address
            IPEndPoint serverEndPoint = new IPEndPoint(ipAddr, serverPort);

            // Connect the socket to server on specified port
            socket.Connect(serverEndPoint);
            isOpen = true;
            Console.WriteLine("Connected to server");
            fsLog.WriteLine("Connected to server\n");
            
        }

        public void Close()
        {
            if (isOpen)
            {
                socket.Close();
                fsLog.WriteLine("connection closed");
                isOpen = false;
            }
        }

        public void Send(string fileName)
        {
            if (!isOpen)
            {
                throw new ApplicationException("Sending file not possible, call Open first");
            }

            try
            {
                using (FileStream fsSource = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    // Read the source file into a byte array.
                    byte[] bytes = new byte[fsSource.Length];
                    int numBytesToRead = (int)fsSource.Length;
                    fsSource.Seek(3, SeekOrigin.Begin);


                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        // Read may return anything from 0 to numBytesToRead.
                        int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);

                        // Break when the end of the file is reached.
                        if (n == 0)
                            break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }

                    // Send the  string to the server
                    socket.Send(bytes, 0, numBytesRead, SocketFlags.None);
                    Console.WriteLine(Encoding.ASCII.GetString(bytes, 0, numBytesRead));
                    fsLog.WriteLine(Encoding.ASCII.GetString(bytes, 0, numBytesRead));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ~Sender()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {    
              
                this.Close();
                socket?.Dispose();
                socket = null;
            }
            //dispose unmanged resource
            fsLog?.Flush();
            fsLog?.Close();
            fsLog?.Dispose();
            fsLog = null;
        }
    }
}
