﻿using _2._1_ExtensionMethodsSimple;
using System;
using System.Collections.Generic;

namespace _2._2_QueryOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 1, 4, 2, 9, 13, 8, 9, 0, -6, 12 };
            string[] cities = { "Bern", "Basel", "Zürich", "Rapperswil", "Genf" };



            /****** HsrMultipleOf ******/

            // Operator 'HsrMultipleOf' / Vielfaches von 4 auf 'numbers'
            Console.WriteLine("---------- \r\nOperator 'HsrMultipleOf' / Vielfaches von 4 auf 'numbers'");
            IEnumerable<int> multiple4 = numbers.HsrMultipleOf(4);
            foreach (int i in multiple4) { Console.WriteLine(i); }

            // Operator 'HsrMultipleOf' / Vielfaches von 2 und 3 auf 'numbers'
            Console.WriteLine("---------- \r\nOperator 'HsrMultipleOf' / Vielfaches von 2 und 3 auf 'numbers'");
            // TODO: Implementieren
            foreach (int i in numbers.HsrMultipleOf(3).HsrMultipleOf(2))
            {
                Console.WriteLine(i);
            }

            /****** HsrWhere ******/

            // Operator 'HsrWhere' / Städtenamen mit 'B' auf 'cities'
            Console.WriteLine("---------- \r\nOperator 'HsrWhere' / Städtenamen mit 'B' auf 'cities'");
            IEnumerable<string> citiesB = cities.HsrWhere(delegate (string s) { return s.StartsWith("B"); });
            foreach (string s in citiesB) { Console.WriteLine(s); }

            // Operator 'HsrWhere' / Städtenamen mit 'e' im Namen und Länge >= 5 auf 'cities'
            Console.WriteLine("---------- \r\nOperator 'HsrWhere' / Städtenamen mit 'e' im Namen und Länge >= 5 auf 'cities'");
            // TODO: Implementieren
            IEnumerable<string> citiesC = cities.HsrWhere(delegate (string s) { return s.Contains("e") && s.Length >= 5; });
            foreach (string s in citiesC)
            {
                Console.WriteLine(s);
            }

            /****** HsrWhere / HsrMultipleOf ******/

            // Operator 'HsrWhere' & 'HsrMultipleOf' / Grösser oder gleich 5 und Vielfaches von 2 auf 'numbers'
            Console.WriteLine("---------- \r\nOperator 'HsrWhere' & 'HsrMultipleOf' / Grösser oder gleich 5 und Vielfaches von 2 auf 'numbers'");
            // TODO: Implementieren
            IEnumerable < int > num2 = numbers.HsrWhere(i => i >= 5).HsrMultipleOf(2);
            foreach(int n in num2)
            {
                Console.WriteLine(n);
            }

            // Operator 'HsrWhere' / Grösser oder gleich 5 und Vielfaches von 2 auf 'numbers'
            Console.WriteLine("---------- \r\nOperator 'HsrWhere' / Grösser oder gleich 5 und Vielfaches von 2 auf 'numbers'");
            // TODO: Implementieren
            IEnumerable<int> num3 = numbers.HsrWhere(i => i >= 5 && i % 2 == 0);
            foreach(int n in num3)
            {
                Console.WriteLine(n);
            }



            Console.ReadKey();
        }
    }
}
