﻿using System;

namespace _1._3_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 1, 2, 3, 4, 5 };
            int[] arr2 = { 1, 3, 5, 7, 9 };

            // TODO: PrintArray für alle Arrays ausführen
            PrintArray(arr1);
            PrintArray(arr2);

            // TODO: Sum für alle Arrays ausführen / Total auf ausgeben
            Console.WriteLine(Sum(arr1));
            Console.WriteLine(Sum(arr2));

            Console.ReadKey();
        }

        // TODO: public static void PrintArray ...
        public static void PrintArray(int[] array)
        {
            Console.WriteLine(string.Format("Total: {0}", array.Length));
            foreach(int num in array)
            {
                Console.Write(string.Format("{0} ", num));
            }
            Console.WriteLine();
        }

        // TODO: public static int Sum ...
        public static int Sum(int[] array)
        {
            int sum = 0;
            for(int i=0; i<array.Length; i++)
            {
                sum += array[i];
            }
            return sum;
        }
    }
}
