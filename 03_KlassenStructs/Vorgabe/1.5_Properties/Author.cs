﻿namespace _1._5_Properties
{
    class Author
    {
        // HINWEIS: Versuchen Sie so wenige Fields wie möglich zu verwenden!

        // TODO: Konstruktur der den Vornamen und Nachnahmen des Autors entgegennimmt.
        public Author(string firstname, string lastname)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            isAnonymous = false;
        }
        
        // TODO: Default-Konstruktor (stellt einen anonymen Autor dar).
        public Author()
        {
            isAnonymous = true;
        }

        // TODO: Readonly Property für Vornamen.
        public string Firstname { get; }

        // TODO: Readonly Property für Nachnamen.
        public string Lastname { get; private set; }


        // TODO: Readonly Property für den vollen Namen (Vor- und Nachnamen kombiniert), Formatierung ist egal
        // Handelt es sich um einen anonymen Autor, soll "anonymous" ausgegeben werden.
        public string Fullname
        {
            get { return IsAnonymous ? "anonymous" : string.Format("{0} {1}", Firstname, Lastname); }
        }

        // TODO: Property um festzustellen, ob der Autor anonym sein soll
        // Wurde der Default-Konstruktur aufgerufen, ist der Autor immer anonym
        // und der Wert darf nicht verändert werden.
        private bool isAnonymous;

        public bool IsAnonymous
        {
            get { return isAnonymous; }
            set
            {
                if(string.IsNullOrWhiteSpace(Firstname) && string.IsNullOrWhiteSpace(Lastname))
                {
                    return;
                }
                isAnonymous = value;
            }
        }
    }
}
