﻿using System;

namespace _1._4_TypeConstraints
{
    // TODO: Versuchen Sie das folgende Template zu instantiieren
    //       Tip: Sie müssen dazu zuerst einen neuen Typen (Klasse) definieren:
    class MyClass<T> where T : MyClass<T>
    {
        /* ... */
    }

    class SubMyClass : MyClass<SubMyClass>
    {

    }

    // TODO: Neue Klasse erzeugen:
    class Auf2
    {
        MyClass<SubMyClass> s = new MyClass<SubMyClass>();
    }

}
