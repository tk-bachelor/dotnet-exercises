﻿using System;

namespace _1._3_Operator
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector v1 = new Vector(10, 20);
            Console.WriteLine("Vector v1: {0}", v1);

            Vector v2 = new Vector(1, 2);
            Console.WriteLine("Vector v2: {0}", v2);

            // TODO: Vector v1 und v2 addieren über Operator + / Resultat auf Konsole ausgeben
            Vector v3 = v1 + v2;
            Print(3, v3);

            // TODO: Vector v1 und v2 subtrahieren über Operator - / Resultat auf Konsole ausgeben
            Vector v4 = v1 - v2;
            Print(4, v4);
            

            // TODO: Vector v1 und v1 vergleichen über Operator ==
            Console.WriteLine("v1==v1 " + (v1 == v1));

            // TODO: Vector v1 und v2 vergleichen über Operator ==
            Console.WriteLine("v1==v2 " + (v1 == v2));

            // TODO: Vector v1 und v1 vergleichen über Operator !=
            Console.WriteLine("v1!=v1 " + (v1 != v1));

            // TODO: Vector v1 und v2 vergleichen über Operator !=
            Console.WriteLine("v1 != v2 " + (v1 != v2));
            Console.ReadKey();
        }

        static void Print(int index, Vector v)
        {
            Console.WriteLine("Vector v{1}: {0}", index, v);
        }
    }
}
