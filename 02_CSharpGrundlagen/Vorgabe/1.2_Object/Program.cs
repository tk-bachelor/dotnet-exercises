﻿using System;

namespace _1._2_Object
{
    class Program
    {
        // TODO: public static string GetStringRepresentation ...
        public static string GetStringRepresentation(object o)
        {
            return o.ToString();
        }

        static void Main(string[] args)
        {
            // TODO: Test mit int / Ausgabe auf Konsole
            string s1 = GetStringRepresentation(123);
            System.Console.WriteLine(s1);

            // TODO Test mit bool / Ausgabe auf Konsole
            string b1 = GetStringRepresentation(false);
            Console.WriteLine(b1);

            // TODO Test mit System.Drawing.Point / Ausgabe auf Konsole
            string p1 = GetStringRepresentation(new System.Drawing.Point(5, 10));
            Console.WriteLine(p1);

            // TODO Test mit Person (siehe unten) / Ausgabe auf Konsole
            Person person = new Person("Keerthikan", "Thurairatnam");

            string p2 = GetStringRepresentation(person);
            Console.WriteLine(p2);

            Console.ReadKey();
        }
    }

    public class Person
    {
        private string firstName;
        private string lastName;
        public Person(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        // TODO: public override string ToString ...
        public override string ToString()
        {
            return string.Format("{0}, {1}", lastName, firstName);
        }
    }
}
