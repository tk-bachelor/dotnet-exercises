﻿using System;

namespace _1._6_Statements
{
    class Program
    {
        static void Main(string[] args)
        {
            // TODO: PromptHeight() verwenden, um Höhe des Baumes (ohne Stamm) einzulesen
            int height = PromptHeight();

            // TODO: Ungültige Eingabe behandeln
            if(height == 0)
            {
                Environment.Exit(0);
            }

            // TODO: Nadelwerk zeichnen
            DrawTree(height);

            // TODO: Stamm zeichnen
            DrawTrunk(height);

            // TODO: Loop implementieren
            for (height = 2; height <= 20; height++)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("-------------------------------------------------");
                Console.WriteLine("Baumhöhe: {0}", height);
                Console.WriteLine();
                Console.WriteLine();
                DrawTree(height);
                DrawTrunk(height);
            }

            Console.ReadKey();
        }

        public static void DrawTree(int height)
        {
            int i = 1;
            while (i <= height)
            {
                for (int n = 1; n <= i; n++)
                {
                    Console.Write("*");
                }
                i++;
                Console.WriteLine();
            }
        }

        public static void DrawTrunk(int height)
        {
            int stemHeight = height / 2;
            int stemWidth = height > 2 ? height / 3 : 1;
            for (int h = 1; h <= stemHeight; h++)
            {
                Console.WriteLine(new String('*', stemWidth));
            }
        }

        private static int PromptHeight()
        {
            Console.Write("Maximale Höhe eingeben 2 - 20: ");

            string heightInput = Console.ReadLine();

            int height;
            if (int.TryParse(heightInput, out height))
            {
                if (height < 2 || height > 20)
                {
                    Console.WriteLine("Ungültiger Wert eingegeben! Die Zahl muss zwischen 2 und 20 liegen!");
                    return 0;
                }
            }
            else
            {
                Console.WriteLine("Ungültiger Wert eingegeben! Kann nicht interpretiert werden!");
                return 0;
            }

            return height;
        }

    }
}
