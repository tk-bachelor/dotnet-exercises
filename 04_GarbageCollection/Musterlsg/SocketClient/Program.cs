﻿using System;              // For String, Int32, Console, ArgumentException
using System.IO;           // For IOException

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // Test for correct # of args
            if ((args.Length < 1) || (args.Length > 3))
            {
                throw new ArgumentException("Parameters: <Server> [<Port>]", nameof(args));
            }

            // Server name or IP address
            string server = args[0];

            // Use port argument if supplied, otherwise default to 1000
            int port = args.Length == 2 
                ? int.Parse(args[1]) 
                : 55555;
            
            try
            {
                using (Sender sender = new Sender())
                {
                    sender.Connect(server, port);
                    while (true)
                    {
                        Console.WriteLine("Enter filename (quit to exit):");
                        var fileName = Console.ReadLine();
                        if (fileName == "quit")
                        {
                            break;
                        }
                        if (File.Exists(fileName))
                        {
                            sender.Send(fileName);
                        }    
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally {
                
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
