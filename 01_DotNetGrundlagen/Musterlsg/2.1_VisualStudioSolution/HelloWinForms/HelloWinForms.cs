using System;
using System.Windows.Forms;

namespace MultipleAssemblies
{
    public class HelloWinForms
    {
        public static void Speak()
        {
            MessageBox.Show("Hello from Windows Forms / HelloWinForms.Speak()");
        }
    }
}