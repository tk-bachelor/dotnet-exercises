﻿using System;
using System.Collections.Generic;

namespace _1._4_Interface
{
    interface IGraphicObject
    {
        void Draw();
    }

    class Kreis : IGraphicObject
    {
        private string name;

        public Kreis(string name) { this.name = name; }

        public void Draw()
        {
            Console.WriteLine("Kreis: {0}", name);
        }
    }
    class Rechteck : IGraphicObject
    {
        private string name;

        public Rechteck(string name) { this.name = name; }

        public void Draw()
        {
            Console.WriteLine("Rechteck: {0}", name);
        }
    }

    class Compound : IGraphicObject
    {
        private string name;
        List<IGraphicObject> objList = new List<IGraphicObject>();

        public Compound(string name) { this.name = name; }

        public void Draw()
        {
            Console.WriteLine("Compound: {0}", name);
            foreach (IGraphicObject g in objList)
            {
                g.Draw();
            }
            Console.WriteLine("End Compound: {0}", name);
        }

        public void Add(IGraphicObject g)
        {
            objList.Add(g);
        }
    }
}
