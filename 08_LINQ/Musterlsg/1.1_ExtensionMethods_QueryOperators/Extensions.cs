﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _1._1_ExtensionMethods_QueryOperators
{
    /// <summary>
    /// Dokumentation der offiziellen LINQ Extension Methods:
    /// http://msdn.microsoft.com/en-us/library/system.linq.enumerable_methods.aspx
    /// 
    /// Nutzen Sie diese Dokumentation um Ihre Extension Methods zu implementieren.
    /// Sie finden dort auch die Beschreibung, was die Methode effektiv für einen Effekt hat.
    /// </summary>
    public static class Extensions
    {
        // TODO: HsrForEach
        public static void HsrForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach (TSource item in source)
                action(item);
        }

        // TODO: HsrWhere
        public static IEnumerable<TSource> HsrWhere<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            foreach (TSource item in source)
                if (predicate(item))
                    yield return item;
        }

        // TODO: HsrOfType
        public static IEnumerable<TResult> HsrOfType<TResult>(this IEnumerable source)
        {
            foreach (object item in source)
                if (item is TResult)
                    yield return (TResult)item;
        }

        // TODO: HsrToList
        public static List<TSource> HsrToList<TSource>(this IEnumerable<TSource> source)
        {
            return new List<TSource>(source);
        }

        // TODO: HsrSum
        public static int HsrSum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            int sum = 0;
            foreach (TSource t in source)
                sum += selector(t);

            return sum;
        }
    }
}
