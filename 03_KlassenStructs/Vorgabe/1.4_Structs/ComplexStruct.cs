﻿namespace _1._4_Structs
{
    struct ComplexStruct
    {
        public int Real { get; set; }
        public int Imaginary { get; set; }

        public ComplexStruct(int real, int imaginary) : this()
        {
            Real = real;
            Imaginary = imaginary;
        }

        public override string ToString()
        {
            return $"Real: {Real} / Imaginary: {Imaginary}";
        }
    }
}
