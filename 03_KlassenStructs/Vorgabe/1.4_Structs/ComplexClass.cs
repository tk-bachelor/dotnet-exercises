﻿namespace _1._4_Structs
{
    class ComplexClass
    {
        public int Real { get; set; }
        public int Imaginary { get; set; }

        public ComplexClass(int real, int imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        public override string ToString()
        {
            return $"Real: {Real} / Imaginary: {Imaginary}";
        }
    }
}
